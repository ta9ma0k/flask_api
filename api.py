#coding:utf-8

from api import create_app

api = create_app()

if __name__=='__main__':
    api.run(port=5000)

