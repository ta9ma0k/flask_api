#coding:utf-8

import csv
import pymysql.cursors
import os
from werkzeug.security import generate_password_hash

#csvpath = os.path.abspath('csv')
user = os.environ.get("USER")
if (user == 'takuma'):
    csvpath = '/home/takuma/usb/python/Flask_dev/FLASK_API/database/csv'
elif(user == 'vagrant'):
    csvpath = '/home/vagrant/flask_api/database/csv'
connection = pymysql.connect(host='localhost',
                             user='root',
                             password='root',
                             db='flaskDB',
                             charset='utf8',
                             cursorclass=pymysql.cursors.DictCursor)

with connection.cursor() as cursor:
    sql = 'SET foreign_key_checks = 0;'
    r = cursor.execute(sql)
    sql = 'TRUNCATE TABLE password'
    r = cursor.execute(sql)
    sql = 'TRUNCATE TABLE follow'
    r = cursor.execute(sql)
    sql = 'TRUNCATE TABLE post'
    r = cursor.execute(sql)
    sql = 'TRUNCATE TABLE users'
    r = cursor.execute(sql)
    sql = 'SET foreign_key_checks = 1;'
    r = cursor.execute(sql)
    connection.commit()

    with open(csvpath + '/user.csv','r',encoding = "utf-8-sig") as f:
        reader = csv.reader(f)
        sql = "INSERT INTO users (name, mailaddress) VALUES (%s, %s)"
        for row in reader:
            r = cursor.execute(sql, (row[0], row[1]))
            print(cursor._executed)
        connection.commit()

    with open(csvpath + '/user.csv','r',encoding = "utf-8-sig") as f:
        i=0
        reader = csv.reader(f)
        sql = "INSERT INTO password (user_id,password) VALUES (%s,%s)"
        for row in reader:
            i+=1
            password_hash = generate_password_hash(row[2])
            r = cursor.execute(sql, (i,password_hash))
            print(cursor._executed)
        connection.commit()

    with open(csvpath + '/follow.csv','r',encoding = "utf-8-sig") as f:
        reader = csv.reader(f)
        sql = "INSERT INTO follow (user_id, follow_id) VALUES (%s, %s)"
        for row in reader:
            r = cursor.execute(sql, (row[0], row[1]))
            print(cursor._executed)
        connection.commit()

    with open(csvpath + '/post.csv','r',encoding = "utf-8-sig") as f:
        reader = csv.reader(f)
        sql = "INSERT INTO post (body,user_id) VALUES (%s, %s)"
        for row in reader:
            r = cursor.execute(sql, (row[1], row[0]))
            print(cursor._executed)
        connection.commit()



