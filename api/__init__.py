#coding:utf-8

from flask import Flask,send_from_directory
import os

def create_app():
    api = Flask(__name__,static_folder='../static/templates')

    api.config.from_object('api.config.Config')

    with api.app_context():
        from .auth import auth as auth_blueprint
        api.register_blueprint(auth_blueprint)

        from .main import main as main_blueprint
        api.register_blueprint(main_blueprint)

    return api