#coding:utf-8

from flask import request,jsonify,Flask,make_response,abort,render_template
import pymysql.cursors
from ..model import Exe_SQL
from . import main
from ..auth.views import login_required

@main.route('/user/all',methods=['GET'])
def Get_AllUser():
    sql = "SELECT users.id,users.name,users.about_me,users.secret_status FROM users"
    results = Exe_SQL(sql)
    status = 200
    return make_response(jsonify(results),status)

@main.route('/user/profile',methods=['GET','POST','PUT','DELETE'])
@login_required
def User(username):
    method = request.method
    if method == 'POST':
        about_me = request.json['about_me']
        if about_me == None:
            abort(400,'Bad Request')
        sql = 'UPDATE users SET users.about_me=%s WHERE users.name=%s'
        sql_arg = (about_me,username)
        status = 201
    elif method == 'DELETE':
        sql = 'DELETE FROM users WHERE users.name=%s'
        sql_arg = None
        status = 204
    elif method == 'PUT':
        secret_status = request.json['secret_status']
        if secret_status == None:
            abort(400,'Bad Request')
        sql = 'UPDATE users SET users.secret_status=%s WHERE users.name=%s'
        sql_arg = (secret_status,username)
        status = 204
    elif method == 'GET':
        status = 200
        sql='SELECT users.id,users.name,users.mailaddress,users.about_me,users.create_time,users.secret_status FROM users WHERE users.name=%s'
        sql_arg = (username)
    results = Exe_SQL(sql,sql_arg)
    return make_response(jsonify(results),status)


@main.route('/user/follow',methods=['GET','POST','DELETE'])
@login_required
def follow(username):
    method = request.method
    if method == 'POST':
        sql_arg = request.json['follow_id']
        if sql_arg == None:
            abort(400,'Bad Request')
        sql = 'INSERT INTO follow(user_id,follow_id) VALUES((SELECT users.id FROM users WHERE users.name=%s),%s)'
        status = 201
    elif method == 'DELETE':
        sql_arg = request.json['follow_id']
        if sql_arg == None:
            abort(400,'Bad Request')
        sql = 'DELETE FROM follow WHERE follow.user_id=(SELECT users.id FROM users WHERE users.name=%s) AND follow.follow_id=%s'
        status = 204
    elif method == 'GET':
        sql_arg = username
        status = 200
        sql = 'SELECT users.id,users.name FROM follow INNER JOIN users WHERE users.id=follow.follow_id AND follow.user_id=(SELECT users.id FROM users WHERE users.name=%s)'
    results = Exe_SQL(sql,sql_arg)

    return make_response(jsonify(results),status)

@main.route('/user/follower',methods=['GET'])
@login_required
def follower(username):
    method = request.method
    if not (method == 'GET'):
        abort(400,'Bad Request')
    sql = 'SELECT users.id,users.name FROM follow INNER JOIN users WHERE users.id = follow.user_id AND follow.follow_id=(SELECT users.id FROM users WHERE users.name=%s)'
    sql_arg = username
    results = Exe_SQL(sql,sql_arg)
    status = 200
    return make_response(jsonify(results),status)

@main.route('/post/all',methods=['GET'])
def all_post():
    method = request.method
    if not (method == 'GET'):
        abort(400,'Bad request')
    sql = "SELECT post.id,post.body,post.user_id,users.name,post.create_time FROM post INNER JOIN users WHERE users.id = post.user_id AND users.secret_status=1 ORDER BY create_time DESC"
    results = Exe_SQL(sql)
    status = 200
    return make_response(jsonify(results),status)

@main.route('/post/own',methods=['GET','POST','DELETE'])
@login_required
def user_post(username):
    method = request.method
    if method == 'POST':
        sql_arg = (username,request.json['body'])
        sql = 'INSERT INTO post(user_id,body) VALUES((SELECT users.id FROM users WHERE users.name=%s),%s)'
        status = 201
    elif method == 'DELETE':
        sql_arg = request.json['post_id']
        sql = 'DELETE FROM post WHERE post.id=%s'
        status = 204
    elif method == 'GET':
        sql_arg = username
        sql = 'SELECT post.id,post.body,post.create_time FROM post WHERE post.user_id=(SELECT users.id FROM users WHERE users.name=%s) ORDER BY create_time DESC'
        status = 200
    results = Exe_SQL(sql,sql_arg)

    return make_response(jsonify(results),status)

@main.route('/post/follower',methods=['GET'])
@login_required
def follower_post(username):
    method = request.method
    if not(method == 'GET'):
        abort(400,'Bad request')
    sql = 'SELECT post.id,post.body,post.create_time,X.follower,X.name FROM post INNER JOIN (SELECT follow.follow_id AS follower,users.name AS name FROM follow INNER JOIN users WHERE users.id = follow.follow_id AND follow.user_id=(SELECT users.id FROM users WHERE users.name=%s)) X WHERE post.user_id = X.follower ORDER BY post.create_time DESC'
    results = Exe_SQL(sql,username)
    status = 200

    return make_response(jsonify(results),status)
