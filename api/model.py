#coding:utf-8

from flask import current_app,abort
import pymysql.cursors
from time import sleep

connection = pymysql.connect(host=current_app.config['DATABASE_HOST'],
                             user=current_app.config['DATABASE_USER'],
                             password=current_app.config['DATABASE_PASSWORD'],
                             db=current_app.config['DATABASE_NAME'],
                             charset=current_app.config['DATABASE_CHARSET'],
                             cursorclass=pymysql.cursors.DictCursor)

def Exe_SQL(sql,sql_arg=None):
    try:
        with connection.cursor() as cursor:
            if sql_arg == None:
                cursor.execute(sql)
            else:
                cursor.execute(sql,sql_arg)
            sleep(0.5)
            results = cursor.fetchall()
            sleep(0.5)
            print(cursor._executed)
        connection.commit()
    except:
        connection.rollback()
        abort(500,'Internal server error')
    return results
