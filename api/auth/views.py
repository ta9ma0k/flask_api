#coding:utf-8

from flask import Flask,request,abort,jsonify,current_app,make_response
from . import auth
from werkzeug.security import check_password_hash,generate_password_hash
import functools
import jwt
import datetime
from ..model import Exe_SQL
import pymysql.cursors

def login_required(method):
    @functools.wraps(method)
    def wrapper(*args, **kwargs):
        header = request.headers.get('Authorization')
        print(header)
        _,token = header.split()
        try:
            decoded = jwt.decode(token,current_app.config['SECRET_KEY'],algorithms='HS256')
            username=decoded['name']
        except jwt.DecodeError:
            abort(400,message='Token is not valid.')
        except jwt.ExpiredSignatureError:
            abort(400,message='Token is expired.')
        return method(username,*args, **kwargs)
    return wrapper

@auth.route('/check',methods=['POST'])
def check_user():
    sql_arg = request.json['name']
    sql = "SELECT count(id) FROM users WHERE users.name=%s"
    results = Exe_SQL(sql,sql_arg)
    if results[0]['count(id)'] == 0:
        response = 0
    else:
        response = 1
    return make_response(jsonify({'result':response}))

@auth.route('/login',methods=['POST'])
def login():
    input_name = request.json['name']
    input_password = request.json['password']
    sql_arg = (input_name)
    sql = "SELECT users.id,users.name,password.password FROM users INNER JOIN password WHERE users.name=%s AND users.id=password.user_id"
    results = Exe_SQL(sql,sql_arg)

    if not check_password_hash(results[0]['password'],input_password):
        return make_response('Password is incorrect',400)

    exp = datetime.datetime.utcnow() + datetime.timedelta(hours=current_app.config['TOKEN_EXPIRE_HOURS'])
    encoded = jwt.encode({'name': input_name, 'user_id':results[0]['id'],'exp': exp}, current_app.config['SECRET_KEY'], algorithm='HS256')
    response = {'user_id':results[0]['id'],'token':encoded.decode('utf-8')}
    return make_response(jsonify(response),200)

@auth.route('/regist',methods=['POST'])
def Regist_User():
    method = request.method
    name = request.json['name']
    mailaddress = request.json['mailaddress']
    password = request.json['password']

    if method != 'POST' or name == None or mailaddress == None or password == None:
        abort(400,'Bad Request')
    sql = 'INSERT INTO users(name,mailaddress) VALUES(%s,%s)'
    sql_arg = (name,mailaddress)
    Exe_SQL(sql,sql_arg)
    sql_arg = (name,generate_password_hash(password))
    sql = 'INSERT INTO password(user_id,password) VALUES((SELECT users.id FROM users WHERE users.name=%s),%s)'
    Exe_SQL(sql,sql_arg)

    return make_response(jsonify({'status':'success'}),201)
